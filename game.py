from random import randint

name = input("Hi! What is your name? ")
month_range = [1, 12]
year_range = [1924, 2004]

for num in range(1, 6):
    print("Guess", num, ":", name, "were you born in", randint(month_range[0], month_range[1]), "/", randint(year_range[0], year_range[1]))
    answer = input("yes or no ")
    if answer == "yes":
        print("I knew it!")
        break
    elif answer == "no":
        if num < 5:
            print("Drat! Lemme try again!")
        if num == 5:
            print("I have other things to do. Good bye.")
